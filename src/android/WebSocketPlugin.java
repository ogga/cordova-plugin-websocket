package fr.ogga.cordova.websocket;

import android.app.Activity;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpClient.WebSocketConnectCallback;
import com.koushikdutta.async.http.WebSocket;
import com.koushikdutta.async.callback.CompletedCallback;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.util.SparseArray;

public class WebSocketPlugin extends CordovaPlugin {

    private SparseArray<WebSocket> _conn;

    @Override
    public void initialize(CordovaInterface cordova, final CordovaWebView webView) {
        super.initialize(cordova, webView);
        _conn = new SparseArray<WebSocket>();
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if ("create".equals(action)) {
            open(args.getInt(0), args.getString(1), callbackContext);
            return true;
        } else if ("send".equals(action)) {
            _conn.get(args.getInt(0)).send(args.getString(1));
        } else if ("close".equals(action)) {
            int id = args.getInt(0);
            if (_conn.indexOfKey(id) >= 0) {
              _conn.get(id).close();
            }
        }
        return false;  // Returning false results in a "MethodNotFound" error.
    }

    private void open(final Integer id, final String uri, final CallbackContext callbackContext) {
        AsyncHttpClient.getDefaultInstance().websocket(uri, null, new WebSocketConnectCallback() {
            @Override
            public void onCompleted(Exception ex, WebSocket webSocket) {
                if (ex != null) {
                    ex.printStackTrace();
                    return;
                }
                _conn.put(id, webSocket);
                webSocket.setStringCallback(new WebSocket.StringCallback() {
                    public void onStringAvailable(String s) {
                        JSONArray response = new JSONArray();
                        try {
                            response.put(0, "T");
                            response.put(1, s);
                        } catch (JSONException e) {
                            Log.e("WebSocket", e.getMessage(), e);
                        }
                        respond(callbackContext, response);
                    }
                });
                webSocket.setClosedCallback(new CompletedCallback() {
                    @Override
                    public void onCompleted(Exception ex) {
                        JSONArray response = new JSONArray();
                        try {
                            response.put(0, "C");
                            response.put(1, ex == null);
                        } catch (JSONException e) {
                            Log.e("WebSocket", e.getMessage(), e);
                        }
                        respond(callbackContext, response, false);
                        _conn.remove(id);
                    }
                });
                JSONArray response = new JSONArray();
                try {
                    response.put(0, "O");
                } catch (JSONException e) {
                    Log.e("WebSocket", e.getMessage(), e);
                }
                respond(callbackContext, response);
            }
        });
    }

    private void respond(final CallbackContext callbackContext, final JSONArray response, final boolean keepCallback) {
        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
              PluginResult result = new PluginResult(Status.OK, response);
              result.setKeepCallback(keepCallback);
              callbackContext.sendPluginResult(result);
            }
        });
    }

    private void respond(final CallbackContext callbackContext, final JSONArray response) {
        respond(callbackContext, response, true);
    }
}
